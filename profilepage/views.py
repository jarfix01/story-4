from django.shortcuts import render

# Create your views here.

def home (request):
    return render(request, 'profilepage/Home.html')

def about (request):
    return render(request, 'profilepage/About.html')

def contact (request):
    return render(request, 'profilepage/Contact.html')

def resume (request):
    return render(request, 'profilepage/Resume.html')